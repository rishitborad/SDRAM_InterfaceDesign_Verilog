module(
input clock,
input [9:0]tWAIT,
input [9:0] tLAT,
input [9:0] tBURST,
input [9:0] tCAS,
input [9:0] tPRE,
input [9:0] LoadtWAIT,
input LoadtBURST,
input LoadtCAS,
input LoadtPRE,
output reg [9:0] CountOut
);

always @(posedge clock) begin
if(LoadtPRE)
CountOut <= tPRE;
else if(LoadtBURST)
CountOut <= tBURST;
else if(LoadtCAS)
CountOut <= tCAS;
else if(LoadWAIT)
CountOut <= tWAIT;
else 
CountOut <= CountOut - 1'b1;
end
endmodule