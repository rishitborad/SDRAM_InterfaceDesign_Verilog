module controllerSD(

input clk,
input reg [1:0] Status,
input Write,
input [3:0]Burst,
input [1:0]Size,
input BIREn,
input wire [9:0]CountOut,
input [9:0]tLAT,
output ready,
output StoreReg,
output EnWData,
output EnRData,
output CS_,
output RAS_,
output CAS_,
output WE_,
output SelRow,
output wire SelCol,
output LoadtPRE,
output LoadtCAS,
output LoadtBURST,
output LoadtWAIT,
output BIWEn,
output BIREn
);

parameter IDLE = 8'b00000000, LOAD_PRE = 8'b00000001, tPRE = 8'b00000010, LOAD_CAS = 8'b00000011, tCAS = 8'b00000100, LOAD_BURST = 8'b00000101, START_WRITE = 8'b00000110, WRITE = 8'b00000111, LOAD_WAIT = 8'b00001000, tWAIT = 8'b00001001, START_READ = 8'b00001010, LAT_WAIT = 8'b00001011, READ = 8'b00001100;

reg [3:0] curr_state;
reg [3:0] next_state;
reg [13:0] CW 

initial
begin
curr_state = IDLE;
next_state = IDLE;
end