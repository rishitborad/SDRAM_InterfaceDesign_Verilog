`define MAX_SIZE 8
`define MAX_LENGTH 8
`define ADDRESS_WIDTH 3

module memoryA(DataOut1,clock,WEA,DataInA);


reg [`ADDRESS_WIDTH-1:0]AddressA;
input WEA;
input [`MAX_LENGTH-1:0] DataInA;
output DataOut1;
output clock;
reg [`MAX_LENGTH-1:0] DataOut1;
//reg [`MAX_LENGTH-1:0] DataInA;
reg [`MAX_LENGTH-1:0] memA[0:`MAX_SIZE-1];
reg [`MAX_LENGTH-1:0] memB[0:3];
reg clock;
//reg [2:0]AddressA;
reg [1:0]AddressB;
reg [0:0]WEB;
reg [`MAX_LENGTH-1:0]D;
reg [`MAX_LENGTH-1:0]Q;
reg [`MAX_LENGTH-1:0]ADDOut;
reg [`MAX_LENGTH-1:0]SUBOut;
reg [`MAX_LENGTH-1:0]DataInB;
reg Sign;
//wire incB = 1'b0;
//*******Clock of the system**************//

initial
	clock=1'b0;
always
	#10 clock =~clock;
initial
	#1000 $finish;

//*******Memory Implementation***********///

initial
begin
	
 	memA[3'b000]=8'b00000001;
	memA[3'b001]=8'b00000010;
	memA[3'b010]=8'b00000011;
	memA[3'b011]=8'b00000100;
	memA[3'b100]=8'b00000101;
	memA[3'b101]=8'b00000110;
	memA[3'b110]=8'b00000111;
	memA[3'b111]=8'b00001000;
	/*
	memA[3'b111]=8'b00000001;
	memA[3'b110]=8'b00000010;
	memA[3'b101]=8'b00000011;
	memA[3'b100]=8'b00000100;
	memA[3'b011]=8'b00000101;
	memA[3'b010]=8'b00000110;
	memA[3'b001]=8'b00000111;
	memA[3'b000]=8'b00001000;
	*/
end

//assign Data = WEA?DataInA:8'bz;
//always @ (posedge clock)
//Address = 3'b000;

integer i;
initial
AddressA = 2'b00;
always @(clock)
	begin:Mem_Read
		if(!WEA)begin					//WEA should be 0 to write
			D <=@(posedge clock) memA[AddressA];
			//D <=memA[AddressA];
			@(clock)AddressA=AddressA+1;
		end	
	end

//********FlipFlop Implementation***********//
always @(clock)
	Q <= @(posedge clock)D;			

//*******COMP**********//
always@(*)
	begin
		if(Q>D)
			Sign <= 0;
		else
			Sign <= 1;
	end

//*******SUBOut & ADDOut********//
always @(*)
	begin
		ADDOut = Q+D;
		SUBOut = Q-D;
		if(Sign)
			DataInB <= ADDOut;
		else
			DataInB <= SUBOut; 
	end

//*******MemB*******//
initial
AddressB = 2'b00;
always@(*)
	begin
		@(posedge clock)if(!WEB)begin
			memB[AddressB] <=DataInB;
			AddressB=AddressB+1;
		end
	end
endmodule	