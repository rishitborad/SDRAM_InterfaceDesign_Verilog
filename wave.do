onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix unsigned /TEST/S123/counter_5
add wave -noupdate /TEST/S123/clk
add wave -noupdate /TEST/S123/rst
add wave -noupdate /TEST/S123/AddrA
add wave -noupdate /TEST/S123/datainA
add wave -noupdate /TEST/S123/DOut1
add wave -noupdate /TEST/S123/DOut2
add wave -noupdate /TEST/S123/AddrB
add wave -noupdate /TEST/S123/DatainB
add wave -noupdate /TEST/S123/WEA
add wave -noupdate /TEST/S123/incA
add wave -noupdate /TEST/S123/WEB
add wave -noupdate /TEST/S123/incB
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {31040 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {133880 ps}
