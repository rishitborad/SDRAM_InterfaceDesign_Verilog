`define MAX_SIZE 8
`define MAX_LENGTH 8
`define ADDRESS_WIDTH 3

module memA(DataInB,AddressA,WEA,DataInA,clock);

input AddressA;
input WEA;
input DataInA;
input clock;
output DataInB;

wire [`ADDRESS_WIDTH-1:0] AddressA;
wire WEA;
wire [`MAX_LENGTH-1:0] DataInA;
reg [`MAX_LENGTH-1:0] DataInB;

reg [`MAX_LENGTH-1:0] memoryA[0:`MAX_SIZE-1];
wire [`MAX_LENGTH-1:0]D;
wire [`MAX_LENGTH-1:0]Q;


//*********Writing into Memory A OR Reading From Memory A********//

always @(clock)
	begin:MemA_write
		if(WEA)
			memoryA[AddressA] = DataInA;
		else
			D <=@(posedge clock) memoryA[AddressA];



//**********Flip-FLOP********//

always @(clock)
	Q <= @(posedge clock)D;	