`define MAX_SIZE 8
`define MAX_LENGTH 8
`define ADDRESS_WIDTH 3

module counterA(AddressA,clock,reset,IncA)

input clock;
input reset;
input IncA;

output AddressA;

reg clock,reset,IncA;
reg AddressA = 3'b000;

always @(*)
	if(reset)
		AddressA = 0'b000;
	else if(incA && ~reset)
		AddressA = AddressA + 1;

endmodule